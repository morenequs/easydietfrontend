# Frontend (Meal plan generator)

Current project supports docker only for development environment.

## Requirements:

- docker
- docker-compose
- linux distribution

## Instalation

### Build docker images

Run command in project directory:
```
sh docker-build.sh
```

### First run
Run commands in project directory:
```
docker-compose up -d
```