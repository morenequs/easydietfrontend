# pull official base image
FROM node:14

ARG USER_ID
ARG GROUP_ID
ARG USER_NAME

RUN : "${USER_ID:?You not specified a USER_ID args. Run docker-build.sh}"
RUN : "${GROUP_ID:?You not specified a GROUP_ID args. Run docker-build.sh}"
RUN : "${USER_NAME:?You not specified a USER_NAME args. Run docker-build.sh}"

WORKDIR /app

RUN groupmod -g 999 node && usermod -u 999 -g 999 node

RUN mkdir -p /home/${USER_NAME} &&\
    chown ${USER_ID}:${GROUP_ID} /home/${USER_NAME} &&\
    chown ${USER_ID}:${GROUP_ID} /app &&\
    groupadd -g ${GROUP_ID} ${USER_NAME} &&\
    useradd -d "/home/${USER_NAME}" -u ${USER_ID} -g ${GROUP_ID} -m -s /bin/bash ${USER_NAME}

USER ${USER_NAME}
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install
RUN yarn global add react-scripts

# add app
COPY . ./

# start app
CMD ["yarn", "start"]