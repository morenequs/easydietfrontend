import React from 'react';
import ReactDOM from 'react-dom';
import Root from "./views/Root";
import {unstable_createMuiStrictModeTheme, ThemeProvider} from '@material-ui/core/styles';
import {CssBaseline} from "@material-ui/core";
import { StylesProvider } from '@material-ui/core/styles';

const theme = unstable_createMuiStrictModeTheme({});

ReactDOM.render(
    <React.StrictMode>
      <StylesProvider injectFirst>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Root/>
      </ThemeProvider>
      </StylesProvider>
    </React.StrictMode>,
    document.getElementById('root')
);