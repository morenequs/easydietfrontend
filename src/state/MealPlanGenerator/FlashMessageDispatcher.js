import {
  SHIFT_FLASH_MESSAGE,
  PUSH_FLASH_MESSAGE
} from "./MealPlanGeneratorContext";

function dispatch(state, action) {
  const {type, payload} = action;

  switch (type) {
    case PUSH_FLASH_MESSAGE: {
      const {messageType, message} = payload;

      return {
        ...state,
        flashMessages: [...state.flashMessages, {messageType, message}]
      };
    }
    case SHIFT_FLASH_MESSAGE: {
      return {
        ...state,
        flashMessages: [...state.flashMessages.slice(1)]
      }
    }
  }

  return state;
}

export const reducerFlashMessage = dispatch;