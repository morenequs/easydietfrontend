import _ from 'lodash';
import {
  ADD_ERROR,
  REMOVE_ERROR
} from "./MealPlanGeneratorContext";
import {MESSAGES} from "../../config/errors";

function dispatch(state, action) {
  const {type, payload} = action;

  switch (type) {
    case ADD_ERROR: {
      const {errorType, data} = payload;

      return {
        ...state,
        errors: {
          ...state.errors,
          [errorType]: {message: MESSAGES[errorType](...data)}
        }
      };
    }
    case REMOVE_ERROR: {
      return {
        ...state,
        errors: _.omit(state.errors, payload)
      };
    }
  }

  return state;
}

export const reducerErrors = dispatch;