import {MESSAGES} from "../../config/errors";
import {
  CHANGE_MEAL_SPLIT_COUNT,
  CHANGE_NUTRIENT_RANGE,
  CHANGE_VISIBILITY_NUTRIENT,
  CHANGE_MEAL_SPLIT
} from "./MealPlanGeneratorContext";

const energyMealSplitDefault = [
  [100],
  [50, 50],
  [30, 40, 30],
  [25, 40, 15, 20],
  [25, 10, 30, 15, 20],
  [25, 10, 30, 10, 15, 10],
]

function dispatch(state, action) {
  const {type, payload} = action;

  console.log(type);
  switch (type) {
    case CHANGE_NUTRIENT_RANGE: {
      const {nutrientType, rangeArray} = payload;
      const nutrients = state.nutrients.map((item) => {
        if (item.type !== nutrientType) {
          return item;
        }
        return {...item, min: rangeArray[0], max: rangeArray[1]};
      })

      return {
        ...state,
        nutrients
      };
    }
    case CHANGE_VISIBILITY_NUTRIENT: {
      const {nutrientType, disabled} = payload;
      const nutrients = state.nutrients.map((item) => {
        if (item.type !== nutrientType) {
          return item;
        }
        return {...item, disabled: disabled};
      })

      return {
        ...state,
        nutrients
      };
    }
    case CHANGE_MEAL_SPLIT_COUNT: {
      if (payload < 1 || payload > energyMealSplitDefault.length) {
        return state;
      }

      return {
        ...state,
        energyMealSplit: energyMealSplitDefault[payload - 1]
      };
    }
    case CHANGE_MEAL_SPLIT: {
      return {
        ...state,
        energyMealSplit: payload
      };
    }
  }

  return state;
}

export const reducerRequirements = dispatch;