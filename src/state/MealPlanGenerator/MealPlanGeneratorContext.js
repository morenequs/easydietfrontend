import React from 'react';

import {ENERGY, PROTEINS, CARBS, FATS} from '../../config/nutrients';
import {reducerRequirements} from "./RequirementsDispatcher";
import {reducerErrors} from "./ErrorsDispatcher";
import {reducerGenerator} from "./GeneratorDispatcher";
import {reducerFlashMessage} from "./FlashMessageDispatcher";

const NAMESPACE_REQUIREMENTS = 'REQUIREMENTS';
const NAMESPACE_ERRORS = 'ERRORS';
const NAMESPACE_GENERATOR = 'GENERATOR';
const NAMESPACE_FLASH_MESSAGE = 'FLASH_MESSAGE';

export const CHANGE_NUTRIENT_RANGE = NAMESPACE_REQUIREMENTS + '/CHANGE_NUTRIENT_RANGE';
export const CHANGE_VISIBILITY_NUTRIENT = NAMESPACE_REQUIREMENTS + '/CHANGE_VISIBILITY_NUTRIENT';
export const CHANGE_MEAL_SPLIT_COUNT = NAMESPACE_REQUIREMENTS + '/CHANGE_MEAL_SPLIT_COUNT';
export const CHANGE_MEAL_SPLIT = NAMESPACE_REQUIREMENTS + '/CHANGE_MEAL_SPLIT';

export const ADD_ERROR = NAMESPACE_ERRORS + '/ADD_ERROR';
export const REMOVE_ERROR = NAMESPACE_ERRORS + '/REMOVE_ERROR';

export const INITIALIZE_SETUP_DATA = NAMESPACE_GENERATOR + '/INITIALIZE_SETUP_DATA';
export const INITIALIZE_GENERATE_MEAL_PLAN = NAMESPACE_GENERATOR + '/INITIALIZE_GENERATE_MEAL_PLAN';
export const START_GENERATE_MEAL_PLAN = NAMESPACE_GENERATOR + '/START_GENERATE_MEAL_PLAN';
export const COMMIT_GENERATE_MEAL_PLAN = NAMESPACE_GENERATOR + '/COMMIT_GENERATE_MEAL_PLAN';
export const FAIL_GENERATE_MEAL_PLAN = NAMESPACE_GENERATOR + '/FAIL_GENERATE_MEAL_PLAN';
export const CHANGE_MEAL_IN_PLAN = NAMESPACE_GENERATOR + '/CHANGE_MEAL_IN_PLAN';
export const CHANGE_REQUIREMENT_DIET_TYPE = NAMESPACE_GENERATOR + '/CHANGE_REQUIREMENT_DIET_TYPE';

export const PUSH_FLASH_MESSAGE = NAMESPACE_FLASH_MESSAGE + '/PUSH_FLASH_MESSAGE';
export const SHIFT_FLASH_MESSAGE = NAMESPACE_FLASH_MESSAGE + '/POP_FLASH_MESSAGE';

const initialState = {
  energyMealSplit: [25, 10, 30, 15, 20],
  nutrients: [
    {type: ENERGY, min: 1800, max: 2000, disabled: false},
    {type: PROTEINS, min: 120, max: 150, disabled: true},
    {type: CARBS, min: 150, max: 200, disabled: true},
    {type: FATS, min: 40, max: 60, disabled: true},
  ],
  setupData: {
    diets: []
  },
  errors: {},
  generator: {
    uuid: null,
    attemptCounter: 0,
    pending: false,
  },
  totalNutrients: {},
  meals: [],
  excludedMeals: [],
  history: [],
  flashMessages: [],
  dietType: ""
}

function dispatch(state, action) {
  const {type} = action;
  console.log(type);

  if (type.lastIndexOf(NAMESPACE_REQUIREMENTS, 0) === 0) {
    return reducerRequirements(state, action);
  }

  if (type.lastIndexOf(NAMESPACE_ERRORS, 0) === 0) {
    return reducerErrors(state, action);
  }

  if (type.lastIndexOf(NAMESPACE_GENERATOR, 0) === 0) {
    return reducerGenerator(state, action);
  }

  if (type.lastIndexOf(NAMESPACE_FLASH_MESSAGE, 0) === 0) {
    return reducerFlashMessage(state, action);
  }

  return state;
}

const StateContext = React.createContext({state: initialState, dispatch: () => 0});

export const Provider = StateContext.Provider;
export const reducer = dispatch;
export const context = StateContext;
export const defaultState = initialState;
export const getByNutrientType = (nutrients, type) => nutrients.find((item) => item.type === type);
