import _ from 'lodash';

import {
  COMMIT_GENERATE_MEAL_PLAN,
  INITIALIZE_GENERATE_MEAL_PLAN,
  START_GENERATE_MEAL_PLAN,
  FAIL_GENERATE_MEAL_PLAN,
  CHANGE_MEAL_IN_PLAN, INITIALIZE_SETUP_DATA, CHANGE_REQUIREMENT_DIET_TYPE
} from "./MealPlanGeneratorContext";

function dispatch(state, action) {
  const {type, payload} = action;

  switch (type) {
    case INITIALIZE_SETUP_DATA: {
      const {diets} = payload;

      return {
        ...state,
        setupData: {...state.setupData, diets}
      };
    }
    case INITIALIZE_GENERATE_MEAL_PLAN: {
      return {
        ...state,
        generator: {...state.generator, pending: true}
      };
    }
    case START_GENERATE_MEAL_PLAN: {
      const {uuid} = payload;

      return {
        ...state,
        generator: {...state.generator, uuid}
      };
    }
    case COMMIT_GENERATE_MEAL_PLAN: {
      const {meals, totalNutrients} = payload;

      console.log('COMMIT_GENERATE_MEAL_PLAN', meals, totalNutrients);

      let history = [];
      if (state.meals.length > 0) {
        history = [
          {meals: state.meals, totalNutrients: state.totalNutrients},
          ...state.history
        ];
      }

      return {
        ...state,
        history: history,
        excludedMeals: [...state.excludedMeals, ...meals.map(({id}) => id)],
        meals: meals,
        totalNutrients: totalNutrients,
        generator: {...state.generator, pending: false, uuid: null}
      };
    }
    case FAIL_GENERATE_MEAL_PLAN: {
      return {
        ...state,
        generator: {...state.generator, pending: false, uuid: null}
      };
    }
    case CHANGE_MEAL_IN_PLAN: {
      const {currentMeal, newMeal} = payload;

      const newTotalNutrients = _.mapValues(state.totalNutrients, (value, key) => {
        const newValue =
            parseFloat(value) - parseFloat(currentMeal.totalNutrients[key]) + parseFloat(newMeal.totalNutrients[key]);

        return newValue.toFixed(3);
      })

      return {
        ...state,
        excludedMeals: [...state.excludedMeals, currentMeal.id],
        totalNutrients: newTotalNutrients,
        meals: state.meals.map((meal) => {
          return meal.id === currentMeal.id ? newMeal : meal;
        })
      };
    }
    case CHANGE_REQUIREMENT_DIET_TYPE:
    {
      const {slug} = payload;

      return {
        ...state,
        dietType: slug
      }
    }
  }

  return state;
}

export const reducerGenerator = dispatch;