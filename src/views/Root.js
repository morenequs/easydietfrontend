import React from 'react';
import MealPlanGenerator from "./MealPlanGenerator/MealPlanGenerator";

const Root = () => {
  return (
    <MealPlanGenerator/>
  );
};

export default Root;
