import React, {useEffect, useReducer} from 'react';
import {
  COMMIT_GENERATE_MEAL_PLAN,
  defaultState, FAIL_GENERATE_MEAL_PLAN,
  INITIALIZE_GENERATE_MEAL_PLAN, INITIALIZE_SETUP_DATA,
  Provider, PUSH_FLASH_MESSAGE,
  reducer, START_GENERATE_MEAL_PLAN
} from "../../state/MealPlanGenerator/MealPlanGeneratorContext";
import MealPlanGeneratorView from "./MealPlanGeneratorView";
import axios from "axios";
import {TRANSLATE} from "../../config/translations";
import {BACKEND_URL, CREATE_MEAL_PLAN, GET_INIT_DATA, MEAL_PLAN} from "../../config/config";

const api = axios.create({
  baseURL: BACKEND_URL
})

const MealPlanGenerator = () => {
  const [state, dispatch] = useReducer(reducer, defaultState);
  const {meals, totalNutrients, history, errors, generator: {pending, uuid}} = state;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await api.get(GET_INIT_DATA);
        dispatch({
          type: INITIALIZE_SETUP_DATA,
          payload: response.data
        })
      } catch (e) {
        dispatch({
          type: PUSH_FLASH_MESSAGE,
          payload: {messageType: 'error', message: TRANSLATE['fail_init_app']}
        })
      }
    }

    fetchData();
  }, []);


  useEffect(() => {
    if (pending && uuid !== null) {
      const id = setInterval(() => {
        checkResultMealPlanGenerateTask(uuid).then();
      }, 500);
      return () => clearInterval(id);
    }
  }, [pending, uuid]);

  const checkResultMealPlanGenerateTask = async (uuid) => {
    try {
      const response = await api.get(MEAL_PLAN + '/' + uuid);
      const data = response.data;

      if (data.type === 'pending') {
        console.log('pending');
        return;
      }

      if (data.type === 'success') {
        dispatch({type: COMMIT_GENERATE_MEAL_PLAN, payload: {...data.mealPlan}});
        dispatch({
          type: PUSH_FLASH_MESSAGE,
          payload: {messageType: 'success', message: TRANSLATE['success_generate_meal_plan']}
        })
        return;
      }

      dispatch({type: FAIL_GENERATE_MEAL_PLAN, payload: {}})
      dispatch({
        type: PUSH_FLASH_MESSAGE,
        payload: {messageType: 'warning', message: TRANSLATE['fail_generate_meal_plan']}
      })
    } catch (e) {
      dispatch({type: FAIL_GENERATE_MEAL_PLAN, payload: {}})
      dispatch({
        type: PUSH_FLASH_MESSAGE,
        payload: {messageType: 'error', message: TRANSLATE['internal_error']}
      })
    }
  }

  const createMealsPlanGenerateTask = () => {
    if (Object.keys(errors).length > 0) {
      return;
    }

    dispatch({type: INITIALIZE_GENERATE_MEAL_PLAN})

    const nutrients = state.nutrients
        .filter((item) => !item.disabled)
        .map(({type, min, max}) => ({type, min, max}));

    const data = {
      energyMealSplit: state.energyMealSplit,
      nutrients,
      excludedMeals: state.excludedMeals,
      dietType: '' !== state.dietType ? state.dietType : null,
    }

    sendMealPlanGenerateTask(data);
  }

  const sendMealPlanGenerateTask = async (data) => {
    try {
      const response = await api.post(CREATE_MEAL_PLAN, data);
      dispatch({type: START_GENERATE_MEAL_PLAN, payload: {uuid: response.data.id}});
    } catch (e) {
      dispatch({type: FAIL_GENERATE_MEAL_PLAN, payload: {}})
      dispatch({
        type: PUSH_FLASH_MESSAGE,
        payload: {messageType: 'error', message: TRANSLATE['internal_error']}
      })
    }
  }

  return (
      <Provider value={{state, dispatch}}>
        <MealPlanGeneratorView
            generatePending={pending}
            errors={errors}
            meals={meals}
            totalNutrients={totalNutrients}
            history={history}
            createMealsPlanGenerateTask={createMealsPlanGenerateTask}
        />
      </Provider>
  );
};

export default MealPlanGenerator;
