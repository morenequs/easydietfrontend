import React from 'react';
import MainTemplates from "../../templates/MainTemplate";
import CenterGridContentTemplate from "../../templates/CenterGridContentTemplate";
import NutrientRequirements from "../../components/organism/MealPlan/NutrientRequirements/NutrientRequirements";
import MealRequirements from "../../components/organism/MealPlan/MealRequirements/MealRequirements";
import BigButton from "../../components/atoms/Button/BigButton";
import styled from "styled-components";
import background from "../../assets/iamge/background.jpg";
import background2 from "../../assets/iamge/background2.jpg";
import background3 from "../../assets/iamge/background3.jpg";
import {withTheme} from "@material-ui/core/styles";
import Paper from "../../components/atoms/Paper/Paper";
import {CircularProgress} from "@material-ui/core";
import MealPlans from "../../components/organism/MealPlan/MealPlans/MealPlans";
import Snackbar from "../../components/molecules/Snackbar/Snackbar";
import TypeDietRequirement from "../../components/organism/MealPlan/TypeDietRequirement/TypeDietRequirement";

const TemplateWrapper = styled.div`
  position: relative;
`

const Background = styled.div`
  overflow: hidden;
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: -1;
`;

const BackgroundWrapper = styled.div`
  position: relative;

  &:before {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    z-index: 100;
    width: 100%;
    height: 150px;
    background: linear-gradient(0deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 1) 100%);
  }

  &:after {
    content: '';
    display: block;
    position: absolute;
    bottom: 0;
    z-index: 100;
    width: 100%;
    height: 150px;
    background: linear-gradient(0deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0) 100%);
  }
`;

const BackgroundImage = styled.img`
  width: 100%;
`;

const StyledMainPaper = withTheme(styled(Paper)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: ${({theme}) => theme.spacing(2)}px;
  background-color: rgba(255, 255, 255, 0.9);
  margin-bottom: 20px;
`);

const Row = styled.div`
  margin: 10px 0;
  flex-basis: 100%;
  display: flex;
  justify-content: center;
  max-width: 800px;
`;

const GenerateButtonWrapper = styled.div`
  margin: 4px;
  position: relative;
`;

const GenerateButtonProgress = styled(CircularProgress)`
  position: absolute;
  top: 50%;
  left: 50%;
  margin-left: -12px;
  margin-top: -12px;
`;

const MealPlanGeneratorView = (
    {
      meals,
      totalNutrients,
      history,
      generatePending,
      errors,
      createMealsPlanGenerateTask
    }) => {
  return (
      <TemplateWrapper>
        <Background>
          <BackgroundWrapper>
            <BackgroundImage src={background}/>
          </BackgroundWrapper>
          <BackgroundWrapper>
            <BackgroundImage src={background2}/>
          </BackgroundWrapper>
          <BackgroundWrapper>
            <BackgroundImage src={background3}/>
          </BackgroundWrapper>
          <BackgroundWrapper>
            <BackgroundImage src={background}/>
          </BackgroundWrapper>
          <BackgroundWrapper>
            <BackgroundImage src={background2}/>
          </BackgroundWrapper>
          <BackgroundWrapper>
            <BackgroundImage src={background3}/>
          </BackgroundWrapper>
        </Background>

        <MainTemplates>
          <CenterGridContentTemplate>
            <StyledMainPaper elevation={1}>
              <Snackbar />
              <Row>
                <NutrientRequirements/>
              </Row>
              <Row>
                <TypeDietRequirement/>
              </Row>
              <Row>
                <MealRequirements/>
              </Row>
              <Row>
                <GenerateButtonWrapper>
                  <BigButton
                      onClick={createMealsPlanGenerateTask}
                      disabled={Object.keys(errors).length > 0 || generatePending}
                      variant="contained"
                      color="primary">
                    Generuj
                  </BigButton>
                  {generatePending && <GenerateButtonProgress size={24}/>}
                </GenerateButtonWrapper>
              </Row>
            </StyledMainPaper>
            {meals.length > 0 && <MealPlans meals={meals} totalNutrients={totalNutrients} history={history}/>}
          </CenterGridContentTemplate>
        </MainTemplates>
      </TemplateWrapper>
  );
};

export default MealPlanGeneratorView;
