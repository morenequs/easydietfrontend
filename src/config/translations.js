export const TRANSLATE = {
  'fail_init_app': 'Nie udało się poprawnie załadować wszystkich danych, spróbuj ponownie później.',
  'success_generate_meal_plan': 'Jadłospis został znaleziony.',
  'fail_generate_meal_plan': 'Niestety nie udało się znaleźć jadłospisu spełniającego zadane wymagania.',
  'internal_error': 'Wystąpił nieoczekiwany błąd. Spróbuj ponownie później.',
  'success_regenerate_meal': 'Znaleziono nowy posiłek dla aktualnego jadłospisu.',
  'fail_regenerate_meal': 'Nie udało się znaleźć nowego posiłku.',
}