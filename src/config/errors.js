export const ERROR_INVALID_ENERGY_SPLIT = 'invalid_energy_split';

export const MESSAGES = {
  [ERROR_INVALID_ENERGY_SPLIT]: val => `Suma pokrycia energii każdego z posiłków powinna być równa 100%, aktualnie wynosi: ${val}.`
}