export const ENERGY = 'energy';
export const PROTEINS = 'proteins';
export const FATS = 'fats';
export const CARBS = 'carbs';

export const NUTRIENTS_CONFIG = {
  [ENERGY]: {
    mainColor: '#4caf50',
    min: 100,
    max: 3500,
    header: "Energia",
    minBias: 150
  },
  [PROTEINS]: {
    mainColor: '#03a9f4',
    min: 0,
    max: 900,
    header: "Białka",
    minBias: 10
  },
  [CARBS]: {
    mainColor: '#f44336',
    min: 0,
    max: 900,
    header: "Węglowodany",
    minBias: 10
  },
  [FATS]: {
    mainColor: '#ffea00',
    min: 0,
    max: 900,
    header: "Tłuszcze",
    minBias: 10
  }
}

export const AVAILABLE_NUTRIENTS = [PROTEINS, CARBS, FATS];