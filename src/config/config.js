export const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

export const GET_INIT_DATA = '/mealPlan/initData';
export const MEAL_PLAN = '/mealPlan';
export const CREATE_MEAL_PLAN = '/mealPlan/create';
