import React from 'react';
import styled from 'styled-components';
import {withTheme} from '@material-ui/core/styles';

const StyledWrapper = withTheme(styled.div`
  padding-top: 20px;
        
  ${props => props.theme.breakpoints.up("md")} {
    padding: 20px 50px;
  }
  
  ${props => props.theme.breakpoints.up("lg")} {
    padding: 20px 150px;
  }
`);

const MainTemplates = ({children}) => {
  return (
      <StyledWrapper>
        {children}
      </StyledWrapper>
  );
};

export default MainTemplates;
