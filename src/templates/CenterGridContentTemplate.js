import React from 'react';
import Grid from '@material-ui/core/Grid';

const CenterGridContentTemplate = ({children}) => {
  return (
      <Grid container justify="center" spacing={3}>
        <Grid item xs={12} sm={10} md={8} lg={8}>
          {children}
        </Grid>
      </Grid>
  );
};

export default CenterGridContentTemplate;
