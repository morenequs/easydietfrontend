import React, {useContext} from 'react';
import _ from 'lodash';
import styled from "styled-components";
import {InputLabel, MenuItem, Select} from "@material-ui/core";
import {withTheme} from "@material-ui/core/styles";
import Paper from "../../../atoms/Paper/Paper";
import FormControl from "../../../atoms/Form/FormControl/FormControl";
import MealConfigRow from "../../../molecules/MealConfigRow/MealConfigRow";
import Typography from "../../../atoms/Typography/Typography";
import {
  ADD_ERROR,
  CHANGE_MEAL_SPLIT,
  CHANGE_MEAL_SPLIT_COUNT,
  context,
  REMOVE_ERROR
} from "../../../../state/MealPlanGenerator/MealPlanGeneratorContext";
import {ERROR_INVALID_ENERGY_SPLIT} from "../../../../config/errors";
import Alert from "../../../atoms/Alert/Alert";

const Wrapper = styled.div`
  width: 100%;
`;

const StyledSelectMealPaper = withTheme(styled(Paper)`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: ${({theme}) => theme.spacing(2)}px;
`);

const SelectWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 20px;
`

const StyledFormControl = styled(FormControl)`
  min-width: 120px;
`;

const MealRequirements = () => {
  const {state: {energyMealSplit, errors}, dispatch} = useContext(context);

  const changeMealSplitCount = (e) => {
    dispatch({
      type: CHANGE_MEAL_SPLIT_COUNT,
      payload: e.target.value
    })
  }

  const setMealEnergyPercent = (index) => (newValue) => {
    const newEnergySplit = energyMealSplit.map((value, inx) => {
      return index === inx ? newValue : value;
    })

    dispatch({
      type: CHANGE_MEAL_SPLIT,
      payload: newEnergySplit
    });

    const energySplitSum = _.sum(newEnergySplit);
    if (energySplitSum !== 100) {
      dispatch({
        type: ADD_ERROR,
        payload: {errorType: ERROR_INVALID_ENERGY_SPLIT, data: [energySplitSum]}
      })
      return;
    }

    if (ERROR_INVALID_ENERGY_SPLIT in errors) {
      dispatch({
        type: REMOVE_ERROR,
        payload: ERROR_INVALID_ENERGY_SPLIT
      })
    }
  }

  return (
      <Wrapper>
        <StyledSelectMealPaper elevation={2}>
          <SelectWrapper>
            <StyledFormControl>
              <InputLabel>Ile posiłków</InputLabel>
              <Select value={energyMealSplit.length} onChange={changeMealSplitCount}>
                <MenuItem value={2}>Dwa</MenuItem>
                <MenuItem value={3}>Trzy</MenuItem>
                <MenuItem value={4}>Cztery</MenuItem>
                <MenuItem value={5}>Pięć</MenuItem>
                <MenuItem value={6}>Sześć</MenuItem>
              </Select>
            </StyledFormControl>
          </SelectWrapper>

          <Typography variant="subtitle2">Wybierz jaką część całej energii powinien orientacyjnie pokrywać każdy z
            posiłków:</Typography>
          {energyMealSplit.map((value, index) =>
              <MealConfigRow
                  key={index}
                  mealNumber={index + 1}
                  value={value}
                  setMealEnergyPercent={setMealEnergyPercent(index)}
              />
          )}
          {ERROR_INVALID_ENERGY_SPLIT in errors &&
          <Alert severity="error">{errors[ERROR_INVALID_ENERGY_SPLIT].message}</Alert>}
        </StyledSelectMealPaper>
      </Wrapper>
  );
};

export default MealRequirements;
