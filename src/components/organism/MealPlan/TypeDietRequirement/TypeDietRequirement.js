import React, {useContext} from 'react';
import styled from "styled-components";
import {withTheme} from "@material-ui/core/styles";
import Paper from "../../../atoms/Paper/Paper";
import FormControl from "../../../atoms/Form/FormControl/FormControl";
import RadioGroup from "../../../atoms/Form/Radio/RadioGroup";
import FormControlLabel from "../../../atoms/Form/FormControl/FormControlLabel";
import Radio from "../../../atoms/Form/Radio/Radio";
import Typography from "../../../atoms/Typography/Typography";
import {
  CHANGE_REQUIREMENT_DIET_TYPE,
  context,
} from "../../../../state/MealPlanGenerator/MealPlanGeneratorContext";

const Wrapper = styled.div`
  width: 100%;
`;

const StyledRow = styled.div`
  margin: 5px 0;
  flex-basis: 100%;
  display: flex;
  justify-content: center;
  max-width: 800px;
`;

const StyledPaper = withTheme(styled(Paper)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: ${({theme}) => theme.spacing(2)}px;
  background-color: rgba(255, 255, 255, 0.9);
  margin-bottom: 20px;
  width: 100%;
`);

const RadioGroupStyled = withTheme(styled(RadioGroup)`
  display: flex;
  justify-content: center;
`);

const TypeDietRequirement = () => {
  const {state: {setupData: {diets}}, dispatch} = useContext(context);

  const handleChange = (event) => {
    dispatch({
      type: CHANGE_REQUIREMENT_DIET_TYPE,
      payload: {slug: event.target.value}
    });
  };

  return (
      <Wrapper>
        <StyledPaper>
          <StyledRow>
            <Typography variant="h5">Dieta:</Typography>
          </StyledRow>
          <StyledRow>
            <FormControl component="fieldset">
              <RadioGroupStyled row aria-label="position" name="position" defaultValue="" onChange={handleChange}>
                <FormControlLabel
                    value=""
                    control={<Radio color="primary"/>}
                    label="Brak"
                    labelPlacement="bottom"
                />
                {diets.map(({id, slug, name}) => (
                  <FormControlLabel
                      key={id}
                      value={slug}
                      control={<Radio color="primary"/>}
                      label={name}
                      labelPlacement="bottom"
                  />
                ))}
              </RadioGroupStyled>
            </FormControl>

          </StyledRow>
        </StyledPaper>
      </Wrapper>
  );
};

export default TypeDietRequirement;
