import React from 'react';
import AccordionSummary from "../../../molecules/Accordion/AccordionSummary";
import AccordionDetails from "../../../molecules/Accordion/AccordionDetails";
import NutrientsSummary from "../NutrientsSummary/NutrientsSummary";
import MealsList from "../MealsList/MealsList";
import {withTheme} from "@material-ui/core/styles";
import styled from "styled-components";
import Paper from "../../../atoms/Paper/Paper";
import Accordion from "../../../molecules/Accordion/Accordion";

const StyledMainPaper = withTheme(styled(Paper)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: ${({theme}) => theme.spacing(2)}px;
  background-color: rgba(255, 255, 255, 0.9);
  margin-bottom: 20px;
`);

const Row = styled.div`
  margin: 10px 0;
  flex-basis: 100%;
  display: flex;
  justify-content: center;
`;

const StyledAccordion = styled(Accordion)`
  width: 100%;
  background-color: rgba(255, 255, 255, 0.7);
`;

const MealPlans = ({meals, totalNutrients, history}) => {
  const [expanded, setExpanded] = React.useState('main');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
      <StyledMainPaper elevation={1}>
        <StyledAccordion expanded={expanded === 'main'} onChange={handleChange('main')}>
          <AccordionSummary>Aktualny plan</AccordionSummary>
          <AccordionDetails>
            <Row>
              <NutrientsSummary totalNutrients={totalNutrients}/>
            </Row>
            <Row>
              <MealsList meals={meals} canRegenerateMeal={true}/>
            </Row>
          </AccordionDetails>
        </StyledAccordion>

        {history.map(({meals, totalNutrients}, index) =>
            <StyledAccordion key={index} expanded={expanded === index} onChange={handleChange(index)}>
              <AccordionSummary>Przepis #{history.length - index}</AccordionSummary>
              <AccordionDetails>
                <Row>
                  <NutrientsSummary totalNutrients={totalNutrients}/>
                </Row>
                <Row>
                  <MealsList meals={meals} canRegenerateMeal={false}/>
                </Row>
              </AccordionDetails>
            </StyledAccordion>
        )}
      </StyledMainPaper>
  );
};

export default MealPlans;
