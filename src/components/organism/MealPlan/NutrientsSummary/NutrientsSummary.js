import React from 'react';
import Paper from "../../../atoms/Paper/Paper";
import {CARBS, ENERGY, FATS, PROTEINS} from "../../../../config/nutrients";
import styled from "styled-components";
import Typography from "../../../atoms/Typography/Typography";

const Wrapper = styled(Paper)`
  padding: 15px;
`;

const NutrientsSummary = ({totalNutrients}) => {
  return (
      <Wrapper>
        <div><Typography variant="h6" component="h3">Energia: {totalNutrients[ENERGY]} kcal</Typography></div>
        <div>Białka: {totalNutrients[PROTEINS]}g</div>
        <div>Węglowodany: {totalNutrients[CARBS]}g</div>
        <div>Tłuszcze: {totalNutrients[FATS]}g</div>
      </Wrapper>
  );
};

export default NutrientsSummary;
