import React from 'react';
import styled from 'styled-components';
import MealCard from "../../../molecules/Card/MealCard/MealCard";

const Wrapper = styled.div`
  max-width: 500px;
  width: 100%;
`

const MealCardWrapper = styled.div`
  margin-bottom: 10px;
`

const MealsList = ({meals, canRegenerateMeal}) => {
  return (
      <Wrapper>
        {meals.map((meal) =>
            <MealCardWrapper key={meal.id}>
              <MealCard meal={meal} canRegenerateMeal={canRegenerateMeal}/>
            </MealCardWrapper>)
        }
      </Wrapper>
  );
};

export default MealsList;
