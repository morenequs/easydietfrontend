import React, {useContext} from 'react';
import RangeSliderNutrient from "../../../molecules/RangeSliderNutrient/RangeSliderNutrient";
import Accordion from "../../../molecules/Accordion/Accordion";
import AccordionSummary from "../../../molecules/Accordion/AccordionSummary";
import AccordionDetails from "../../../molecules/Accordion/AccordionDetails";
import {withTheme} from "@material-ui/core/styles";
import styled from "styled-components";
import Checkbox from "../../../atoms/Form/Checkbox/Checkbox";
import {AVAILABLE_NUTRIENTS, ENERGY, NUTRIENTS_CONFIG} from "../../../../config/nutrients";
import {
  CHANGE_NUTRIENT_RANGE, CHANGE_VISIBILITY_NUTRIENT,
  context,
  getByNutrientType
} from "../../../../state/MealPlanGenerator/MealPlanGeneratorContext";

const Wrapper = styled.div`
  width: 100%;
`;

const StyledRow = styled.div`
  margin: 5px 0;
  flex-basis: 100%;
  display: flex;
  justify-content: center;
  max-width: 800px;
`;

const StyledNutrientRow = withTheme(styled.div`
  display: flex;
  flex-wrap: nowrap;
`);

const StyledCheckboxNutrient = styled(Checkbox)`
  padding: 25px;
`;

const StyledAccordion = styled(Accordion)`
  width: 100%;
`;

const NutrientRequirements = () => {
  const {state: {nutrients}, dispatch} = useContext(context);

  const setRange = (nutrientType) => (value) => {
    dispatch({
      type: CHANGE_NUTRIENT_RANGE,
      payload: {nutrientType: nutrientType, rangeArray: value}
    })
  }

  const setVisibilityNutrient = (nutrientType) => (disabled) => {
    dispatch({
      type: CHANGE_VISIBILITY_NUTRIENT,
      payload: {nutrientType: nutrientType, disabled: disabled}
    })
  }

  const energyData = getByNutrientType(nutrients, ENERGY);

  const nutrientsConfig = AVAILABLE_NUTRIENTS.map((type) => {
    const data = getByNutrientType(nutrients, type);

    return {
      type,
      range: [data.min, data.max],
      disabled: data.disabled,
      setRange: setRange(type),
      setVisibilityNutrient: setVisibilityNutrient(type),
      color: NUTRIENTS_CONFIG[type].mainColor,
      min: NUTRIENTS_CONFIG[type].min,
      max: NUTRIENTS_CONFIG[type].max,
      header: NUTRIENTS_CONFIG[type].header,
      minBias: NUTRIENTS_CONFIG[type].minBias,
    };
  });

  return (
      <Wrapper>
        <StyledRow>
          <RangeSliderNutrient
              type={ENERGY}
              range={[energyData.min, energyData.max]}
              disabled={energyData.disabled}
              setRange= {setRange(ENERGY)}
              header="Energia:"
              color={NUTRIENTS_CONFIG[ENERGY].mainColor}
              min={NUTRIENTS_CONFIG[ENERGY].min}
              max={NUTRIENTS_CONFIG[ENERGY].max}
              minBias={NUTRIENTS_CONFIG[ENERGY].minBias}
              big={true}
          />
        </StyledRow>
        <StyledRow>
          <StyledAccordion>
            <AccordionSummary>Mikroskładniki</AccordionSummary>
            <AccordionDetails>
              {nutrientsConfig.map((nutrient) => {
                return (
                    <StyledNutrientRow key={nutrient.type}>
                      <StyledCheckboxNutrient
                          checked={!nutrient.disabled}
                          onChange={(e) => nutrient.setVisibilityNutrient(!e.target.checked)}/>
                      <RangeSliderNutrient {...nutrient}/>
                    </StyledNutrientRow>
                )
              })}
            </AccordionDetails>
          </StyledAccordion>
        </StyledRow>
      </Wrapper>
  );
};

export default NutrientRequirements;
