import React from 'react';
import {AccordionSummary as MuiAccordionSummary} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const AccordionSummary = ({children}) => {
  return (
      <MuiAccordionSummary
        expandIcon={<ExpandMoreIcon />}
      >
        {children}
      </MuiAccordionSummary>
  );
};

export default AccordionSummary;
