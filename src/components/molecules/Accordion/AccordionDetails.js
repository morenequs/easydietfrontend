import React from 'react';
import styled from "styled-components";
import {AccordionDetails as MuiAccordionDetails} from "@material-ui/core";

const Wrapper = styled.div`
  padding: 16px;
`;

const AccordionDetails = ({children}) => {
  return <Wrapper>{children}</Wrapper>;
};

export default AccordionDetails;
