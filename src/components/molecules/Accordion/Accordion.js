import React from 'react';
import {Accordion as MuiAccordion} from "@material-ui/core";
import styled from "styled-components";

const StyledAccordion = styled(MuiAccordion)`
  width: 100%;
`;

const Accordion = ({children}) => {
  return (
      <StyledAccordion>
        {children}
      </StyledAccordion>
  );
};

export default MuiAccordion;
