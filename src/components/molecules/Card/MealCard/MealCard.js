import React, {Fragment} from 'react';
import Card from "../Card";
import CardHeader from "../CardHeader";
import IconButton from "../../../atoms/Button/IconButton";
import CardContent from "../CardContent";
import Typography from "../../../atoms/Typography/Typography";
import CardActions from "../CardActions";
import Collapse from "../../Collapse/Collapse";
import styled from "styled-components";
import CardMedia from "../CardMedia";
import withTheme from "@material-ui/core/styles/withTheme";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import RegenerateMeal from "./RegenerateMeal";

const StyledCardMedia = styled(CardMedia)`
  height: 200px;
`;

const StyledExpandMoreIconButton = withTheme(styled(IconButton)`
  margin-left: auto;
  transform: ${props => props.expanded ? 'rotate(180deg)' : 'rotate(0deg)'};
  transition: ${({theme}) => theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  })};
`);

const PrepareTimeWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const PrepareTimeTextWrapper = styled(Typography)`
  margin-left: 10px;
`;

const StepWrapper = styled.div`
  border-bottom: 1px solid #666666;
  margin-bottom: 15px;
  padding-bottom: 10px;
`;

const List = styled.ul`
  list-style-type: none;
  padding-left: 0;
  text-transform: lowercase;
`;

const SpanWrapper = styled.span`
  color: rgba(0, 0, 0, 0.54);
`;

const MealCard = ({meal, canRegenerateMeal}) => {
  const {id, name, steps, prepTime, image, mealProducts} = meal;

  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
      <div>
        <Card>
          <CardHeader
              title={name}
              action={canRegenerateMeal && <RegenerateMeal id={id}/>}
          />
          <StyledCardMedia
              // image={`https://source.unsplash.com/collection/1016/700x400?sig=${id}`}
              image={image}
              title="Food"
          />
          <CardContent>
            <PrepareTimeWrapper>
              <AccessTimeIcon/>
              <PrepareTimeTextWrapper variant="body2" color="textSecondary" component="p">Czas przygotowania: {prepTime}min</PrepareTimeTextWrapper>
            </PrepareTimeWrapper>
            <List>
                {mealProducts.map((mealProduct) =>
                    <li key={mealProduct.id}>{mealProduct.name} <SpanWrapper>({mealProduct.gram}g)</SpanWrapper></li>,
                )}
            </List>
          </CardContent>
          <CardActions disableSpacing>
            <StyledExpandMoreIconButton
                aria-label="show more"
                onClick={handleExpandClick}
                expanded={expanded ? 1 : 0}
            >
              <ExpandMoreIcon/>
            </StyledExpandMoreIconButton>
          </CardActions>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Fragment>
                {steps.map((step, index) =>
                    <StepWrapper key={index}>
                      <Typography variant="h5" component="h5">Krok {index + 1}</Typography>
                      <Typography variant="body2">{step}</Typography>
                    </StepWrapper>
                )}
              </Fragment>
            </CardContent>
          </Collapse>
        </Card>
      </div>
  );
};

export default MealCard;
