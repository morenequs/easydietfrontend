import axios from "axios";
import React, {useContext} from 'react';
import IconButton from "../../../atoms/Button/IconButton";
import RefreshIcon from '@material-ui/icons/Refresh';
import {
  CHANGE_MEAL_IN_PLAN,
  context, PUSH_FLASH_MESSAGE
} from "../../../../state/MealPlanGenerator/MealPlanGeneratorContext";
import {BACKEND_URL} from "../../../../config/config";
import {TRANSLATE} from "../../../../config/translations";

const api = axios.create({
  baseURL: `${BACKEND_URL}meal/generateByRequirement`
})

const RegenerateMeal = ({id}) => {
  const {state: {totalNutrients, meals, nutrients, excludedMeals}, dispatch} = useContext(context);

  const handleClick = async () => {
    const currentMeal = meals.find((meal) => meal.id === id);
    const nutrientsRequirement = nutrients
        .filter((item) => !item.disabled)
        .map(({type, min, max}) => {
          const newMin = parseFloat(min) + parseFloat(currentMeal.totalNutrients[type]) - parseFloat(totalNutrients[type]);
          const newMax = parseFloat(max) + parseFloat(currentMeal.totalNutrients[type]) - parseFloat(totalNutrients[type]);
          return {type: type, min: Math.round(newMin.toFixed(3)), max: Math.round(newMax.toFixed(3))}
        });

    try {
      const response = await api.post('', {
        energyMealSplit: [100],
        excludedMeals,
        nutrients: nutrientsRequirement
      });

      dispatch({
        type: CHANGE_MEAL_IN_PLAN,
        payload: {
          currentMeal: currentMeal,
          newMeal: response.data
        }
      })

      dispatch({
        type: PUSH_FLASH_MESSAGE,
        payload: {messageType: 'success', message: TRANSLATE['success_regenerate_meal']}
      })
    } catch (e) {
      dispatch({
        type: PUSH_FLASH_MESSAGE,
        payload: {messageType: 'error', message: TRANSLATE['fail_regenerate_meal']}
      })
    }
  }

  return (
      <IconButton onClick={handleClick}>
        <RefreshIcon/>
      </IconButton>
  );
};

export default RegenerateMeal;
