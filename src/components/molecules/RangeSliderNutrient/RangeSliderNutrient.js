import React, {useState} from 'react';
import Slider from "../../atoms/Slider/Slider";
import styled from "styled-components";
import {Box, Typography} from "@material-ui/core";
import ValueBox from "../../atoms/ValueBox/ValueBox";
import withWidth, {isWidthDown} from '@material-ui/core/withWidth';
import BigSlider from "../../atoms/Slider/BigSlider";

const StyledNutrientBox = styled(Box)`
  width: 100%;
  max-width: 800px;
`;

const StyledRow = withWidth()(styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: ${props => isWidthDown("sm", props.width) || props.big ? 'wrap' : 'nowrap'};
`);

const SliderWrapper = styled.div`
  margin-right: 20px;
  width: 100%;
`

const RangeSliderNutrient = (
    {
      header,
      color,
      range,
      setRange,
      disabled = false,
      min = 100,
      max = 3000,
      minBias = 150,
      big = false
    }) => {

  const [state, setState] = useState(range);

  const handleChange = (value) => {
    setState(value);
    // setRange(value);
  }

  const onChangeCommitted = () => {
    let currentMin = state[0];
    let currentMax = state[1];

    if (currentMin < min) currentMin = min;
    if (currentMax > max) currentMax = max;

    if (currentMin > currentMax) {
      const tmp = currentMin;
      currentMin = currentMax;
      currentMax = tmp;
    }

    if (currentMax - currentMin < minBias) {
      if (currentMax - minBias >= min) {
        currentMin = currentMax - minBias;
      } else {
        currentMax = min + minBias;
      }
    }

    setRange([currentMin, currentMax]);
    setState([currentMin, currentMax]);
  }

  const sliderProps = {
    onChange: handleChange,
    value: state,
    onChangeCommitted: onChangeCommitted,
    min: min,
    max: max,
    color: color,
    disabled: disabled
  };

  const SliderComponent = big ? BigSlider : Slider;

  return (
      <StyledNutrientBox>
        <Typography variant="subtitle2" component="h3">{header}</Typography>
        <StyledRow big={big}>
          <SliderWrapper>
            <SliderComponent {...sliderProps}/>
          </SliderWrapper>
          <ValueBox disabled={disabled} rangeArray={state} onChange={handleChange} onChangeCommitted={onChangeCommitted}/>
        </StyledRow>
      </StyledNutrientBox>
  );
};

export default RangeSliderNutrient;
