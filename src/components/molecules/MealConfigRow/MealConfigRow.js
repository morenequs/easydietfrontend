import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Slider from "../../atoms/Slider/Slider";
import PercentNumberInput from "../../atoms/Form/Input/PercentNumberInput";
import Paper from "../../atoms/Paper/Paper";
import Typography from "../../atoms/Typography/Typography";

const Wrapper = styled(Paper)`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  padding: 15px 25px;
  margin: 10px;
`;

const InputWrapper = styled.div`
  width: 50px;
  margin-left: 10px;
`;

const HeaderWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const SliderWrapper = styled.div`
  width: 100%;
  display: flex;
`

const MealConfigRow = ({value, mealNumber, setMealEnergyPercent}) => {
  const [state, setState] = useState(value);

  useEffect( () => {
    setState(value);
  }, [value])

  const onChange = (val) => {
    if (val < 0 || val > 100) {
      setState(state);
      return;
    }

    setState(val);
  }

  const onChangeCommitted = () => {
    setMealEnergyPercent(state);
  }

  return (
      <Wrapper elevation={3}>
        <HeaderWrapper><Typography variant="body1">Posiłek {mealNumber}</Typography></HeaderWrapper>
        <SliderWrapper>
          <Slider color="#1de9b6" min={0} max={100} onChange={onChange} onChangeCommitted={onChangeCommitted} value={state}/>
          <InputWrapper><PercentNumberInput onChange={onChange} onChangeCommitted={onChangeCommitted} value={state}/></InputWrapper>
        </SliderWrapper>
      </Wrapper>
  );
};

export default MealConfigRow;
