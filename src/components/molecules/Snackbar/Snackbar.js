import React, {useContext, useState} from 'react';
import {Snackbar as MuiSnackbar} from '@material-ui/core';
import {
  context,
  SHIFT_FLASH_MESSAGE
} from "../../../state/MealPlanGenerator/MealPlanGeneratorContext";
import Alert from "../../atoms/Alert/Alert";

const Snackbar = () => {
  const {state: {flashMessages}, dispatch} = useContext(context);

  const [open, setOpen] = useState(false);
  const [messageInfo, setMessageInfo] = useState(undefined);
  const [messageType, setMessageType] = useState('success');

  React.useEffect(() => {
    if (flashMessages.length && !messageInfo) {
      setMessageInfo(flashMessages[0].message);
      setMessageType(flashMessages[0].messageType);
      dispatch({type: SHIFT_FLASH_MESSAGE});
      setOpen(true);
    } else if (flashMessages.length && messageInfo && open) {
      setOpen(false);
    }
  }, [flashMessages, messageInfo, messageType, open]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const handleExited = () => {
    setMessageInfo(undefined);
    setMessageType(undefined);
  };

  return (
      <MuiSnackbar open={open} autoHideDuration={4000} onClose={handleClose} onExited={handleExited}>
        <Alert onClose={handleClose} severity={messageType} elevation={6} variant="filled">
          {messageInfo}
        </Alert>
      </MuiSnackbar>
  );
};

export default Snackbar;
