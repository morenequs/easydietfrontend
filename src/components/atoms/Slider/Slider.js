import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Slider as MuiSlider} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    color: props => props.color,
    height: 8,
  },
});

const Slider = ({value, onChange, onChangeCommitted, min, max, disabled, ...props}) => {
  const classes = useStyles(props);
  const handleChange = (e, value) => onChange(value);

  return (
      <MuiSlider
          className={classes.root}
          value={value}
          onChange={handleChange}
          onChangeCommitted={onChangeCommitted}
          aria-labelledby="range-slider"
          step={1}
          min={min}
          max={max}
          disabled={disabled}
      />
  );
};

export default Slider;
