import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {Slider as MuiSlider} from '@material-ui/core';

const StyledSlider = withStyles({
  root: {
    color: '#ff9800',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(MuiSlider);

const BigSlider = ({value, onChange, onChangeCommitted, min, max, disabled}) => {
  const handleChange = (e, value) => onChange(value);

  return (
      <StyledSlider
          value={value}
          onChange={handleChange}
          onChangeCommitted={onChangeCommitted}
          aria-labelledby="range-slider"
          step={1}
          min={min}
          max={max}
          disabled={disabled}
      />
  );
};

export default BigSlider;