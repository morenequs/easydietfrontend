import React from 'react';
import styled from 'styled-components';
import SimpleNumberInput from "../Form/Input/SimpleNumberInput";

const StyledValueBox = styled.div`
  padding: 10px 10px;
  border: 1px solid;
  border-radius: 3px;
  width: 150px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const StyledInput = styled(SimpleNumberInput)`
  max-width: 20px;
  text-align: right;
`

const Dash = styled.div`
  font-size: 22px;
  padding: 0 5px;
`;

const ValueBox = ({rangeArray, onChange, onChangeCommitted, disabled}) => {
  const handleChange = (index) => (value) => {
    const range = [rangeArray[0], rangeArray[1]];
    range[index] = value;
    onChange(range);
  }

  return (
      <StyledValueBox>
        <StyledInput disabled={disabled} value={rangeArray[0]} onChange={handleChange(0)} onBlur={onChangeCommitted}/>
        <Dash>-</Dash>
        <StyledInput disabled={disabled} value={rangeArray[1]} onChange={handleChange(1)} onBlur={onChangeCommitted}/>
      </StyledValueBox>
  );
};

export default ValueBox;
