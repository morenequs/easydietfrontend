import React from 'react';
import styled from 'styled-components';
import {FormControl, InputAdornment, TextField} from "@material-ui/core";

const StyledNumberInput = styled(TextField)`
  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  
  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
`;

const SimpleNumberInput = ({disabled, value, onChange, onBlur}) => {
  const handleChange = (e) => {
    onChange(parseInt(e.target.value) || 0);
  }

  return (
      <FormControl>
        <TextField
            value={disabled ? '' : value.toString()}
            onChange={handleChange}
            onBlur={onBlur}
            disabled={disabled}
        />
      </FormControl>
  );
};

export default SimpleNumberInput;
