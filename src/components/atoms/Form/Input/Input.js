import React from 'react';
import {TextField} from "@material-ui/core";

const Input = () => {
  return (
      <TextField
        id="outlined-number"
        label="Number"
        type="number"
        InputLabelProps={{
          shrink: true,
        }}
        variant="outlined"
      />
  );
};

export default Input;
