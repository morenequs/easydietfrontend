import React from 'react';
import styled from 'styled-components';
import {FormControl, InputAdornment, Input} from "@material-ui/core";

const StyledNumberInput = styled(Input)`
  /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  
  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
`;

const PercentNumberInput = ({disabled, value, onChange, onChangeCommitted}) => {
  const handleChange = (e) => {
    onChange(parseInt(e.target.value) || 0);
  }

  return (
      <FormControl>
        <StyledNumberInput
            type="number"
            value={disabled ? '' : value.toString()}
            onChange={handleChange}
            onBlur={onChangeCommitted}
            disabled={disabled}
            endAdornment={<InputAdornment position="end">%</InputAdornment>}
        />
      </FormControl>
  );
};

export default PercentNumberInput;