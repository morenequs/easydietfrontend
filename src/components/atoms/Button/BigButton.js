import React from 'react';
import Button from "./Button";
import styled from "styled-components";

const StyledButton = styled(Button)`
  padding: 15px 25px;
`;

const BigButton = ({children, ...props}) => {
  return (
      <StyledButton {...props}>{children}</StyledButton>
  );
};

export default BigButton;
